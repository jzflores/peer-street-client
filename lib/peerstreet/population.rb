module PeerStreet
  class Population < Model
    fields :zip, :cbsa, :name, :pop_estimate_2015, :pop_estimate_2014
  end
end