module PeerStreet
  class Model
    attr_reader :client, :attributes, :prefix

    def self.fields(*names)
      return @fields if names.empty?

      @fields ||= []

      names.each do |name|
        define_method name do
          @attributes[name.to_sym]
        end

        define_method "#{name}=" do |value|
          @attributes[name.to_sym] = value
        end

        @fields.push(name.to_sym)
      end
    end

    def initialize(client, attributes)
      @client = client
      @attributes = {}
      @prefix = attributes.delete(:prefix)
      process(attributes)
    end

    def process(attributes)
      self.class.fields.each do |field|
        if attributes.has_key?(field) || attributes.has_key?(field.to_s)
          @attributes[field] = attributes[field] || attributes[field.to_s]
        end
      end
      self
    end
  end
end