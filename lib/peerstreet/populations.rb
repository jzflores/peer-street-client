module PeerStreet
  class Populations < CollectionProxy
    path '/populations/growth'

    def grownth(options = {})
      response = client.request(:get, path, {:query => {:zip => options[:zip]}})
      Population.new(client, response)
    end
  end
end