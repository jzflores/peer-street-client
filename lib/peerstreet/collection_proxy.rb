module PeerStreet
  class CollectionProxy
    include Enumerable

    attr_accessor :client, :prefix

    def initialize(client, prefix = nil)
      self.client = client
      self.prefix = prefix
    end

    def self.path(value = nil)
      return @path unless value
      @path = value
    end

    def self.model(value = nil)
      @model ||= PeerStreet.const_get(to_s.split("::").last.sub(/s$/, ''))
    end

    def model
      self.class.model
    end

    def path
      [prefix, self.class.path].compact.join("/")
    end
  end
end