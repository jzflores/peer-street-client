require 'faraday'
require 'json'

module PeerStreet
  class Client
    ENDPOINT = 'http://peer-street.herokuapp.com'
    RETRIES = 5

    attr_accessor :endpoint

    def initialize(options = {})
      self.endpoint = options[:endpoint] || ENDPOINT
    end

    def populations
      Populations.new(self)
    end

    def request(verb, path, options={})
      retries = 0
      begin
        conn = Faraday.new(:url => self.endpoint)
        case verb
        when :get
          response = conn.get ::File.join(self.endpoint, '/api', path), options[:query]
          JSON.parse(response.body)
        end
      rescue Faraday::Error::ConnectionFailed, Faraday::Error::TimeoutError => e
        if retry_request?(verb, e.response && e.response.status, retries)
          retries += 1
          retry
        else
          raise ConnectionError, message_for(e, 'Connection Error')
        end
      end
    end

    private

    def retry_request?(http_verb, status_code, retries)
      return false unless [:get].include?(http_verb.to_s.downcase.to_sym)
      return retries < RETRIES unless status_code && status_code == 400
    end

    def message_for(error, default)
      error.message.strip == "" ? default : error.message
    end

  end
end