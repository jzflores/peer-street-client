# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'peerstreet/version'  

Gem::Specification.new do |spec|
  spec.name          = "peerstreet"
  spec.version       = PeerStreet::VERSION
  spec.authors       = ["Juan Flores"]
  spec.summary       = %q{Gem to wrap peer street API}

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake"

  spec.add_dependency "faraday"
  spec.add_dependency "json"
end