## Setup


Install dependencies

```
bundle
```

## Usage

```
rake console

2.4.1 :001 > client = PeerStreet::Client.new()
 => #<PeerStreet::Client:0x007fb7aab112c8 @endpoint="http://peer-street.herokuapp.com">

2.4.1 :002 > p1 = client.populations.grownth({:zip=>90266})
=> #<PeerStreet::Population:0x007fb7aab59f50 @client=#<PeerStreet::Client:0x007fb7aab112c8 @endpoint="http://peer-street.herokuapp.com">, @attributes={:zip=>90266, :cbsa=>31080, :name=>"Los Angeles-Long Beach-Anaheim, CA", :pop_estimate_2015=>13340068, :pop_estimate_2014=>13254397}, @prefix=nil>
 
2.4.1 :003 > p1.name
=> "Los Angeles-Long Beach-Anaheim, CA"
 
2.4.1 :004 > p1.pop_estimate_2015
=> 13340068
 
2.4.1 :005 > p1.zip
=> 90266
 
2.4.1 :006 > p1.cbsa
=> 31080
 
2.4.1 :007 > p2 = client.populations.grownth({:zip=>88340})
=> #<PeerStreet::Population:0x007fb7ae064470 @client=#<PeerStreet::Client:0x007fb7aab112c8 @endpoint="http://peer-street.herokuapp.com">, @attributes={}, @prefix=nil>
 
```